
### What is this repository for? ###

The Weather Year for Passive Performance Assessment (WYPPA) is produced to support the evaluation of passive systems and natural ventilation in buildings. The WYPPA format differs from other typical and reference weather years formats as greater weighting is given to the climatic variables that influence the performance of passive systems.
### License ###

![Alt text](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)


This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License.](http://creativecommons.org/licenses/by-nc/4.0/)

### Attribution ###

 The WYPPA climate file was generated by [Flux Consultants](https://www.fluxco.co/) based on the year weather records generated by [Exemplary Energy](http://www.exemplary.com.au/) which in turn was generated from weather data supplied by the [Bureau of Meteorology](http://www.bom.gov.au/).
 Flux Consultants methodology is available in this repository under the file WYPPA Methodology Statement.pdf. Exemplary's methodology and copyright provisions are located [here](http://www.exemplary.com.au/download/Lee%20-%20Paper%201298%20-%20Climate%20Data%20For%20Building%20Optimisation%20for%20IBPSA%202011.pdf) for reference. 

### Version Information ###
#### Au_Mascot_WYPPA_V1 ####
   WMO: 94767 
   
   Source years: 2000 to 2017
   
   Relevant component weighting factors: 
   
*  Coincident wind speed and direction - 0.50
*  Coincident dry bulb temperature and moisture content - 0.25
*  Coincident direct normal radiation and time of day - 0.25

### Formats Provided ###

WYPAA file provided in [energyplus](https://energyplus.net/sites/default/files/pdfs_v8.3.0/AuxiliaryPrograms.pdf), [EDSL TAS](https://www.edsltas.com/) and Australian Climatic Data Bank formats.



